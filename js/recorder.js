(function ($, window, Drupal, once) {
  'use strict';

  Drupal.behaviors.audiorecorder = {
    attach: function (context, settings) {

      // Rewrite any audio file links as players for preview.
      const elements = once('audiorecorder', '.form-audio-recorder-file .file--audio', context);
      elements.forEach(function (element) {
        var fileLinkWrapper = $(element);
        var fileLink = $('a', fileLinkWrapper).first();
        var preview = $('<audio controls src="">');
        preview.attr("src", fileLink.attr("href"));
        fileLinkWrapper.after(preview);
        fileLinkWrapper.hide();
      });

      // Test if the browser supports recording with vmsg, falling back to
      // the native file-upload if not.
      var AudioContext = window.AudioContext || window.webkitAudioContext;
      if (!AudioContext) {
        return;
      }

      // Don't bother loading vmsg if there aren't any recorder elements.
      if ($(".js-form-type-audio-recorder-file input.form-file", context).length < 1) {
        return;
      }

      import(settings.audiorecorder.base_path + 'libraries/vmsg/vmsg.js')
        .then(vmsg => {

          // Hide our file-upload input and replace it with the recording UI.
          const elements = once('audiorecorder', '.js-form-type-audio-recorder-file input.form-file', context)
          elements.forEach(function (element) {
            var uploadInput = $(element);

            // Hide the upload input.
            uploadInput.hide();

            // Hide the file-upload restrictions as these aren't applicable
            // when using the recorder UI.
            $(".form-audio-recorder-file", context).siblings('.description').each(function (index) {
              // Upload restrictions will be 3 '<br>' elements each followed by
              // text. Remove these last 6 child-nodes.
              var lastIndex = this.childNodes.length - 1;
              var breaksRemoved = 0;
              for (var i = lastIndex; i >= 0 && breaksRemoved < 3; i--) {
                var childNode = this.childNodes[i];
                if (childNode.nodeName == 'BR') {
                  breaksRemoved++;
                }
                this.removeChild(childNode);
              }
            });

            var recordButton = $('<button class="button">' + Drupal.t('Record') + '</button>');
            uploadInput.after(recordButton);

            var preview = $('<audio controls src="">');
            preview.hide();
            uploadInput.after(preview);

            recordButton.click(function () {
              vmsg.record({
                wasmURL: settings.audiorecorder.base_path + 'libraries/vmsg/vmsg.wasm'
              }).then(blob => {

                // Set up the preview.
                var url = URL.createObjectURL(blob);
                preview.attr("src", url);
                preview.show();
                recordButton.text(Drupal.t("Record Again"));

                // Set our hidden input field to the base-64 value of the recorded
                // data so that it is submitted on POST. The value will look like:
                //   data:audio/mpeg;base64,//uQxAAAAAAAAAAAAAAA...VVU=
                // The file inputs themselves are read-only, so we can't modify and
                // upload the blob directly with them.
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                  // Check for a hidden input as a direct sibling of the record
                  // button.
                  if (uploadInput.siblings('input.audio_recorder_file-recording').length) {
                    uploadInput.siblings('input.audio_recorder_file-recording').first().val(reader.result);
                  }
                  // If not, check for a sibling of our parent element.
                  else {
                    uploadInput.parent().siblings('input.audio_recorder_file-recording').first().val(reader.result);
                  }
                }

              });

              // Attach a timer to stop recording after our max time. The vmsg
              // Form object isn't directly accessible, so we'll attach events
              // to the buttons in the DOM once they exist.
              if (settings.audiorecorder.max_recording_time > 0) {
                var observer = new MutationObserver(function (records) {
                  records.forEach(function (record) {
                    for (var i = 0; i < record.addedNodes.length; i++) {
                      var node = record.addedNodes[i];
                      if (node.classList && node.classList.contains('vmsg-record-button')) {
                        // Once the record-button has been added the DOM, attach
                        // a timer when clicked to stop recording after our
                        // max-recording-time.
                        $('.vmsg-record-button').click(function (e) {
                          setTimeout(function () {
                            $('.vmsg-stop-button').trigger('click');
                          }, settings.audiorecorder.max_recording_time * 1000);
                        });
                      }
                    };
                  });
                });
                observer.observe(document.body, { childList: true, subtree: true });
              }

              return false;
            });
          });

        })
        .catch(err => {
          console.log(err.message);
        });
    }
  };

}(jQuery, window, Drupal, once));
