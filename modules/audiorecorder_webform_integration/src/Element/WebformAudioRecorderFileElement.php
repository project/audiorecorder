<?php

namespace Drupal\audiorecorder_webform_integration\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\audiorecorder\Element\AudioFileRecorder;

/**
 * A wrapper for the audio_recorder_file element.
 *
 * @FormElement("webform_audio_recorder_file_element")
 */
class WebformAudioRecorderFileElement extends AudioFileRecorder {

  /**
   * {@inheritdoc}
   */
  public static function processManagedFile(&$element, FormStateInterface $form_state, &$complete_form) {
    parent::processManagedFile($element, $form_state, $complete_form);

    // Here you can add and manipulate your element's properties and callbacks.
    // If the form element does not have
    // an upload control, skip this.
    if (!isset($element['upload'])) {
      return $element;
    }

    $extensions = $element['#file_extensions'];
    $supported_extensions = ['mp3'];

    // We just always add mp3.
    $extensions = !empty($extensions) ? array_intersect(explode(' ', $extensions), $supported_extensions) : $supported_extensions;

    // Replace the upload HTML element with our own recording element.
    $element['upload'] = [
      '#type' => 'audio_recorder_file',
      '#submit_element' => "[name={$element['upload_button']['#name']}]",
      '#upload_validators' => [
        'file_validate_extensions' => implode(' ', $extensions),
      ],
      '#accept' => 'audio/*',
      '#max_recording_time' => $element['#max_recording_time'],
    ];
    // Add the recording UI Javascript & CSS.
    $element['#attached']['library'][] = 'audiorecorder/audiorecorder.recorder';
    $element['#attached']['drupalSettings']['audiorecorder'] = [
      'max_recording_time' => $element['#max_recording_time'],
    ];
    return $element;
  }

}
