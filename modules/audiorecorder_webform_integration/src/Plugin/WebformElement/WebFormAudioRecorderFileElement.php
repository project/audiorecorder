<?php

namespace Drupal\audiorecorder_webform_integration\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformAudioFile;

/**
 * Provides a 'webform_audio_recorder_file_element' element.
 *
 * @WebformElement(
 *   id = "webform_audio_recorder_file_element",
 *   label = @Translation("Audio recorder"),
 *   description = @Translation("Allows to record audio from the browser."),
 *   category = @Translation("File upload elements"),
 *   states_wrapper = TRUE,
 *   dependencies = {
 *     "file",
 *   }
 * )
 */
class WebFormAudioRecorderFileElement extends WebformAudioFile {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      'max_recording_time' => 45,
    ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['file']['file_extensions']['#suffix'] = $this->t('Audiorecorder will always add <strong>mp3</strong>, no matter the extensions chosen here.');
    $form['file']['file_preview']['#suffix'] = $this->t('Audiorecorder will always show the HTML5 audio player (yet).');
    $form['file']['file_preview']['#disabled'] = TRUE;
    $form['audiorecorder'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Audiorecorder'),
    ];
    $form['audiorecorder']['max_recording_time'] = [
      '#type' => 'textfield',
      '#attributes' => [
        ' type' => 'number',
      ],
      '#size' => 10,
      '#title' => $this->t('Maximum Recording Time (seconds)'),
      '#description' => $this->t('The maximum recording time allowed (in seconds). Use 0 for unlimited time.'),
      '#required' => TRUE,
    ];
    return $form;
  }

}
