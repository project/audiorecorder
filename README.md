# Audio Recorder Widget

Provides a field widget for file-fields that uses the vmsg HTML5/JavaScript
library for in-browser audio recording.

See Middlebury's [name-pronunciation documentation](https://mediawiki.middlebury.edu/LIS/CourseHub_name_pronunciation)
for an example of how this field widget can be used.

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Troubleshooting

## Requirements

This module requires no modules outside of Drupal core, however it does require
the vmsg npm asset.

## Recommended modules
- [Webform](https://www.drupal.org/project/webform): This module includes an
  **Audio Recorder Webform Integration** submodule that allows the audio
  recorder widget to be used in Webforms.

## Installation

1. Update your project's root `composer.json` to support the installation of
   the vmsg library dependency to the correct location in your
   Drupal file-structure.

   *  Add `npm-asset/vmsg` as a `composer` repository to your
      project's root `composer.json` similar to this
      (`repositories` can either be an array or a map):

      ```
      "repositories": {
         "drupal": {
             "type": "composer",
             "url": "https://packages.drupal.org/8"
         },
      +   "npm-asset/vmsg": {
      +       "type": "package",
      +       "package": {
      +           "name": "npm-asset/vmsg",
      +           "version": "0.4.0",
      +           "type": "drupal-library",
      +           "dist": {
      +               "url": "https://registry.npmjs.org/vmsg/-/vmsg-0.4.0.tgz",
      +               "type": "tar"
      +           }
      +       }
      +   }
      }
      ```

      According to the [Composer documentation](https://getcomposer.org/doc/05-repositories.md):

      > "Repositories are only available to the root package
      > and the repositories defined in your dependencies will not be loaded."

      It is also possible to use alternative packaging methods to define to
      Composer how to download and install NPM libraries to the Drupal
      `libraries/` directory. Asset-packagist used to be the method to
      accomplish this, but has at least temporarily gone offline (2022) and
      might not be trustworthy.

   *  You may need to require the `oomphinc/composer-installers-extender`
      package which provides the ability to define install-paths for types of
      dependencies as described in
      [Using Composer to Install Drupal and Manage Dependencies --> Downloading third-party libraries using Composer](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#third-party-libraries).

      In your project's root `composer.json` add lines under `extra` that define
      where you want the libraries to be installed:

      ```
      "extra": {

          "installer-paths": {
              "web/core": ["type:drupal-core"],
              "web/libraries/{$name}": [
                "type:drupal-library"
              ]
              "web/modules/contrib/{$name}": ["type:drupal-module"],
              "web/modules/custom/{$name}": ["type:drupal-custom-module"],
              "web/profiles/contrib/{$name}": ["type:drupal-profile"],
              "web/themes/contrib/{$name}": ["type:drupal-theme"],
              "web/themes/custom/{$name}": ["type:drupal-custom-theme"]
          },
      },
      ```

      Note that your project *may* use a different directory structure and
      and webserver configuration. The important thing is that the vmsg
      libraries get installed along-side other Drupal libraries so that they
      can be properly located by Drupal and your webserver.

2. Add the module and its vmsg dependency with composer in your Drupal project:
   `composer require drupal/audiorecorder:^1.0`.


3. The vmsg library uses web-assembly, so you must tell
   your webserver to use the appropriate content-type for
   `.wasm` files in your `.htaccess`:

   ```
   # Add correct encoding for Web-Assembly.
   AddType application/wasm wasm
   ```

4. If you find that your composer workflow is resulting in the file-permissions
   that are preventing your webserver from reading the `vmsg.js` and `vmsg.wasm`
   files, you can address this by adding something like the following to your
   project's root `composer.json`. This generally should be safe, but in your
   server environment it may be preferable to change read access only for your
   webserver user. See [Issue #3197357](https://www.drupal.org/project/audiorecorder/issues/3197357)
   for more details.

   ```
   "scripts": {
       "post-install-cmd": [
           "chmod a+r web/libraries/vmsg/vmsg.*"
       ]
   },
   ```

## Configuration

 1. Enable the module at Administration > Extend.
 2. Add a file field to an entity. Set the "Allowed file extensions" to `mp3`.
    Set a reasonable maximum upload size as browsers that don't support the
    audio-recording APIs will display a file-upload widget instead of the
    recording UI.
 3. In the "Manage Form Settings" tab for the entity, choose "Audio Recorder" as
    the widget for your file field. You can also set a maximum recording time.

## Troubleshooting

### Browser can't load libraries

If you find that your composer workflow is resulting in the file-permissions
that are preventing your webserver from reading the `vmsg.js` and `vmsg.wasm`
files, you can address this by adding something like the following to your
project's root `composer.json` then re-running `composer install`.

```
    "scripts": {
        "post-install-cmd": [
            "chmod a+r web/libraries/vmsg/vmsg.*"
        ]
    },
```
This generally should be safe, but in your server environment it may be
preferable to change read access only for your webserver user.

See [Issue #3197357](https://www.drupal.org/project/audiorecorder/issues/3197357)
for more details.

## Maintainers
 - Adam Franco - [adamfranco](https://www.drupal.org/u/adamfranco)
