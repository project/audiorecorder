<?php

namespace Drupal\audiorecorder\Plugin\Field\FieldWidget;

use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\audiorecorder\Element\AudioFileRecorder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A recorder widget for audio file fields.
 *
 * @FieldWidget(
 *   id = "file_audio_recorder",
 *   label = @Translation("Audio Recorder"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class AudioRecorderWidget extends FileWidget implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * Constructs an AudioRecorderWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Render\ElementInfoManagerInterface $element_info
   *   The element info manager service.
   * @param Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The translation service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ElementInfoManagerInterface $element_info, TranslationInterface $stringTranslation) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $element_info);
    $this->stringTranslation = $stringTranslation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('element_info'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'max_recording_time' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['max_recording_time'] = [
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => $this->t('Maximum Recording Time (seconds)'),
      '#default_value' => $this->getSetting('max_recording_time'),
      '#description' => $this->t('The maximum recording time allowed (in seconds). Use 0 for unlimited time.'),
      '#weight' => 10,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if (empty($this->getSetting('max_recording_time'))) {
      $summary[] = $this->t('Max Recording time: unlimited');
    }
    else {
      $summary[] = $this->t('Max Recording time: @max_recording_time seconds', ['@max_recording_time' => $this->getSetting('max_recording_time')]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['#field_settings'] = $this->getFieldSettings();
    $maxRecordingTime = $this->getSetting('max_recording_time');
    if (empty($maxRecordingTime) || !is_numeric($maxRecordingTime) || $maxRecordingTime < 0) {
      $element['#field_settings']['max_recording_time'] = 0;
    }
    else {
      // Ensure it is a float / int.
      $element['#field_settings']['max_recording_time'] = ($maxRecordingTime + 0);
    }

    // Add a class to our container to allow selecting of children.
    // We'll need this to replace the file-link with an audio-player preview.
    $element['#attributes']['class'][] = 'form-audio-recorder-file';

    return $element;
  }

  /**
   * Form API callback: Processes a file_audio_recorder field element.
   *
   * Expands the file_audio_recorder type to include the max_recording_time.
   *
   * This method is assigned as a #process callback in formElement() method.
   */
  public static function process($element, FormStateInterface $form_state, $form) {
    $element = parent::process($element, $form_state, $form);

    // If the form element does not have
    // an upload control, skip this.
    if (!isset($element['upload'])) {
      return $element;
    }

    $field_settings = $element['#field_settings'];
    $extensions = $field_settings['file_extensions'];
    $supported_extensions = ['mp3'];

    // If using custom extension validation. Otherwise, validate against
    // supported extensions.
    $extensions = !empty($extensions) ? array_intersect(explode(' ', $extensions), $supported_extensions) : $supported_extensions;

    // Replace the upload HTML element with our own recording element.
    $element['upload'] = [
      '#type' => 'audio_recorder_file',
      '#submit_element' => "[name={$element['upload_button']['#name']}]",
      '#upload_validators' => [
        'file_validate_extensions' => implode(' ', $extensions),
      ],
      '#accept' => 'audio/*',
      '#max_recording_time' => $field_settings['max_recording_time'],
    ];
    // Add the recording UI Javascript & CSS.
    $element['#attached']['library'][] = 'audiorecorder/audiorecorder.recorder';
    $element['#attached']['drupalSettings']['audiorecorder'] = [
      'max_recording_time' => $field_settings['max_recording_time'],
      'base_path' => base_path(),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function value($element, $input, FormStateInterface $form_state) {
    if ($input) {
      // Checkboxes lose their value when empty.
      // If the display field is present make sure its unchecked value is saved.
      if (empty($input['display'])) {
        $input['display'] = $element['#display_field'] ? 0 : 1;
      }
    }

    // We depend on the file element to handle uploads.
    $return = AudioFileRecorder::valueCallback($element, $input, $form_state);
    // If no new recordings were added, use the parent to handle delete.
    if (empty($return)) {
      return parent::value($element, $input, $form_state);
    }

    // Ensure that all the required properties are returned even if empty.
    $return += [
      'fids' => [],
      'display' => 1,
      'description' => '',
    ];

    return $return;
  }

}
