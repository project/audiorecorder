<?php

namespace Drupal\audiorecorder\Element;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Element\ManagedFile;

/**
 * Provides an upload and recording widget for uploading and saving files.
 *
 * @see ManagedFile::getInfo()
 *
 * @FormElement("audio_recorder_file")
 */
class AudioFileRecorder extends ManagedFile {

  /**
   * {@inheritdoc}
   */
  public static function processManagedFile(&$element, FormStateInterface $form_state, &$complete_form) {
    $element = parent::processManagedFile($element, $form_state, $complete_form);

    // Add a hidden field for our recording data to be injected.
    $element['recording'] = [
      '#type' => 'hidden',
      '#attributes' => ['class' => ['audio_recorder_file-recording']],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $return = parent::valueCallback($element, $input, $form_state);

    // If we have a recording, create a file for it.
    if (empty($return['fids']) && !empty($return['upload']['recording'])) {

      // Prepare the destination if needed.
      // Copied from file_managed_file_save_upload().
      $destination = $element['#upload_location'] ?? NULL;
      if (isset($destination) && !\Drupal::service('file_system')->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY)) {
        \Drupal::logger('audio_recorder')->notice(
          'The upload directory %directory for the file field %name could not be created or is not accessible. A newly uploaded file could not be saved in this directory as a consequence, and the upload was canceled.', [
            '%directory' => $destination,
            '%name' => $element['#field_name'],
          ]
        );
        $form_state
          ->setError($element, t('The file could not be uploaded.'));
        return $return;
      }

      if (preg_match('/^data:([^;]+);base64,(.*)$/', $return['upload']['recording'], $m)) {
        $mimeType = $m[1];
        $data = base64_decode($m[2]);
        unset($m);
        if ($mimeType == 'audio/mpeg') {
          $extension = '.mp3';
        }
        else {
          $extension = '';
        }
        $name = 'recording';
        $location = $element['#upload_location'] . '/' . $name . $extension;
        $file_repository = \Drupal::service('file.repository');
        $file = $file_repository->writeData($data, $location);
        if ($file) {
          $file->setMimeType($mimeType);
          $file->save();
          $return['fids'][] = $file->id();
        }
        else {
          \Drupal::logger('audio_recorder')->warning(
            'Error saving file to %location for %name.', [
              '%location' => $location,
              '%name' => $element['#field_name'],
            ]
          );
          $form_state->setError(
            $element,
            t('Recording for the @name field were unable to be saved.', [
              '@name' => $element['#title'],
            ])
          );
        }
      }
    }

    return $return;
  }

}
